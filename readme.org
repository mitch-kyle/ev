* ev

ev is a small utility for reading and writing edn and json as well as performing
camel-snake-kebab transformations on the map keys.

** Dependencies

- [[https://github.com/babashka/babashka#installation][babashka]]

** Installation

1. Install [[https://github.com/babashka/babashka#installation][babashka]]
2. Download the [[https://codeberg.org/mitch-kyle/ev/raw/branch/main/bin/ev][ev script]]
3. place it on your ~$PATH~ (e.g. ~/usr/local/bin~)
4. make it executable (e.g. ~sudo chmod a+x /usr/local/bin/ev~)

** Usage

*** Pretty Printing

#+begin_example
# pretty print an edn file
ev --pretty example.edn
ev -p example.edn

# pretty print json converted from an edn file
ev -p -j example.edn

# save results to a file
ev -p example.edn > pretty-example.edn
ev -p example.edn -o pretty-example.edn
#+end_example

*** JSON to EDN

#+begin_example
# reading json from a file and convert to edn
ev --from-json example.json
ev -J example.json

# reading json from standard input and convert to edn
cat example.json | ev --from-json
#+end_example

*** EDN to JSON

#+begin_example
# reading edn from a file and convert to json
ev --to-json example.edn
ev -j example.edn

# reading edn from standard input and convert to json
cat example.json | ev --to-json
#+end_example

*** Key Case and Type Conversions

#+begin_example
# changing the case while preserving the type
echo '{:fooBar :baz}' | ev --key-case kebab-case
=> {:foo-bar :baz}

# changing the type while preserving the case
echo '{:fooBar :baz}' | ev --key-type string
=> {"fooBar" :baz}

# changing the case and the type of the keys
echo '{:fooBar :baz}' | ev --key-case SCREAMING_SNAKE_CASE --key-type symbol
=> {FOO_BAR :baz}

# Key conversions work on json but in json all indentity types become strings anyway
echo '{"fooBar":"bar"}' | ev --key-case PascalCase --to-json --from-json
=> {"FooBar":"bar"}"
#+end_example

*** Options

| Option              | Description                                                            |
|---------------------+------------------------------------------------------------------------|
| -c, --key-case CASE | Convert the keys into to given case. See "Key Case Conversions" below. |
| -t, --key-type TYPE | Convert the keys into to given type. (keyword, symbol, string)         |
| -J, --from-json     | Parse input as JSON instead of EDN.                                    |
| -j, --to-json       | Emit JSON instead of EDN.                                              |
| -o, --output FILE   | Send output to a file rather than standard output.                     |
| -a, --append        | Append to output file rather than overwriting it.                      |
| -p, --pretty        | Pretty print the resulting object.                                     |
| -n, --newline       | Add a newline to the end of the output.                                |
| -h, --help          | Print usage information.                                               |

**** Key Case Conversions

- ~SCREAMING_SNAKE_CASE~
- ~kebab-case~
- ~Camel_Snake_Case~
- ~camelCase~
- ~HTTP-Header-Case~
- ~snake_case~
- ~PascalCase~

** For Developers

Running the tests:

#+begin_example
bb test
#+end_example

Building the project:

 #+begin_example
bb build
 #+end_example
