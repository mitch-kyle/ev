(ns build
  (:require [babashka.fs :as fs]
            [babashka.process :refer [shell]]
            [clojure.test :as test]
            [clojure.edn :as edn]))

(def executable-name  "ev")
(def script           "src/ev.clj")

(def output-dir       "bin")
(def scripts-dir      "scripts")
(def src-dir          "src")
(def test-dir         "test")
(def install-dir      (or (System/getenv "DEST_DIR") "/usr/local/bin"))

(defn build []
  (fs/create-dirs output-dir)
  (let [src  (fs/file script)
        dest (fs/file output-dir executable-name)]
    (when (->> (fs/glob src-dir "**.clj")
               (cons src)
               (fs/modified-since dest)
               seq)
      (println (str "Building: " src " -> " dest))
      (shell "bb" "uberscript" dest "-f" src)
      ;; Add shebang interpreter and make it executable
      (spit dest (str "#!/usr/bin/env bb\n" (slurp dest)))
      (fs/set-posix-file-permissions dest "rwxr-xr-x"))))

(defn test []
  (let [test-namespaces (map #(->> % str slurp edn/read-string second)
                             (fs/glob test-dir "**.clj"))]
    (doseq [test-ns test-namespaces]
      (require test-ns))
    (let [{:keys [error fail]} (apply test/run-tests test-namespaces)]
      (System/exit (if (and (zero? error) (zero? fail)) 0 1)))))

(defn clean []
  (fs/delete-tree output-dir))

(defn install []
  (-> (fs/file output-dir executable-name)
      (fs/copy install-dir)))

(defn uninstall []
  (->> (fs/file install-dir executable-name)
       fs/delete-if-exists))
