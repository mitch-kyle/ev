(ns ev
  (:require [camel-snake-kebab.core]
            [camel-snake-kebab.extras :as cske]
            [cheshire.core :as json]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pprint]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]))

(def type-conversions
  {"keyword"  keyword
   "symbol"   symbol
   "string"   (fn [x]
                (if (keyword? x)
                  (if-let [ns (namespace x)]
                    (str ns \/ (name x))
                    (name x))
                  (str x)))
   "preserve" identity})

(def case-conversions
  (into {"preserve" identity}
        (keep (fn [[sym var]]
                (let [s (str sym)]
                  (when (re-seq #"^->.*case$" (str/lower-case s))
                    [(subs s 2) @var]))))
        (ns-publics 'camel-snake-kebab.core)))

(defn json-str
  [{:keys [pretty newline]} x]
  (str (json/generate-string x {:pretty (when pretty {:indent-arrays? true})})
       (when newline \newline)))

(defn edn-str
  [{:keys [pretty newline]} x]
  (with-out-str ((cond pretty  pprint/pprint
                       newline prn
                       :else   pr)
                 x)))

(defn main [& args]
  (let [usage-header (str"\n  ev - ev [OPTIONS] [FILE]\n\n"
                         "Print edn FILE to standard output. If no file is "
                         "given then read from standard input. Apply case and "
                         "type conversions to the map keys. input and output "
                         "as JSON supported.\n\nOptions:\n\n")

        {:keys [arguments errors summary options]}
        (cli/parse-opts
         args
         [["-c" "--key-case CASE"
           (str "Convert the keys into to given case. "
                "See \"Key Case Conversions\" below.")
           :parse-fn     case-conversions
           :validate     [some?
                          (apply str "key case conversion must be one of:\n"
                                 (->> case-conversions
                                      keys
                                      (interpose \newline)))]]
          ["-t" "--key-type TYPE"
           "Convert the keys into to given type. (keyword, symbol, string)"
           :parse-fn     type-conversions
           :validate     [some?
                          (apply str "key type conversion must be one of:\n"
                                 (->> type-conversions
                                      keys
                                      (interpose \newline)))]]
          ["-J" "--from-json" "Parse input as JSON instead of EDN."]
          ["-j" "--to-json" "Emit JSON instead of EDN."]
          ["-o" "--output FILE" "Send output to a file rather than stdout."]
          ["-a" "--append" "Append to output file rather than overwriting it."]
          ["-p" "--pretty" "Pretty print the resulting object."]
          ["-n" "--newline" "Add a newline to the end of the output."]
          ["-h" "--help" "Print usage information."]])

        usage (->> [usage-header
                    summary
                    "\n\nExamples:\n"
                    "\n  - JSON to EDN:\n\n"
                    "    cat example.json | ev --from-json\n"
                    "    ev --from-json example.json\n"
                    "\n  - EDN to JSON:\n\n"
                    "    cat example.edn | ev --to-json\n"
                    "    ev --to-json example.edn\n"
                    "\n  - Pretty Printing:\n\n"
                    "    ev --pretty example.edn\n"
                    "    ev --pretty --to-json example.edn\n"
                    "    ev --pretty example.edn > pretty-example.edn\n"
                    "\n  - Key Conversion:\n\n"
                    "    echo '{:foo :bar}' | ev --key-case "
                    "SCREAMING_SNAKE_CASE --key-type symbol\n"
                    "    => {FOO :bar}\n\n"
                    "    echo '{\"foo\":\"bar\"}' | ev --key-case "
                    "PascalCase --to-json --from-json\n"
                    "    => {\"Foo\":\"bar\"}\n"
                    "\nKey Case Conversions:\n\n"
                    "  See: https://clj-commons.org/camel-snake-kebab/ for"
                    " more information\n\n"
                    (->> case-conversions
                         keys
                         (map (partial str "  - "))
                         (interpose \newline))
                    "\n\nKey Type Conversions:\n\n"
                    (->> type-conversions
                         keys
                         (map (partial str "  - "))
                         (interpose \newline))
                    \newline]
                   flatten
                   (apply str))
        help? (or (:help options) (= "help" (first arguments)))]
    (cond
      errors (do (println (str (->> errors (interpose \newline) (apply str))
                               "\n\n"
                               usage))
                 (System/exit 1))
      help?  (println usage)
      :else  (let [{:keys [append key-case key-type output from-json to-json]}
                   options

                   input   (first arguments)
                   keys-xf (comp (or key-type identity) (or key-case identity))
                   parse   (if from-json json/parse-string edn/read-string)
                   encode  (if to-json json-str edn-str)]
               (let [text (->> (or input *in*)
                               slurp
                               parse
                               (cske/transform-keys keys-xf)
                               (encode options))]
                 (if output
                   (with-open [wtr (io/writer (io/file output)
                                              :append append)]
                     (binding [*out* wtr]
                       (print text)))
                   (print text)))))))

;; TODO find or make a better check for repl vs script
(def repl? (nil? (System/getProperty "babashka.file")))

(when-not repl? (apply main *command-line-args*))
