(ns ev-test
  (:require [babashka.fs :as fs]
            [cheshire.core :as json]
            [clojure.edn :as edn]
            [clojure.java.shell :refer [sh]]
            [clojure.pprint :as pprint]
            [clojure.test :as test :refer [deftest is are testing]]
            [ev])
  (:import (java.io StringReader)))

(def ^:private ev-bin    "bin/ev")
(def ^:dynamic *tmp-dir* nil)

(test/use-fixtures :once
  (fn [f]
    (assert (fs/exists? ev-bin) "the build has to run before the tests")
    (binding [*tmp-dir* (fs/create-temp-dir)]
      (f)
      (fs/delete-tree *tmp-dir*))))

(defn ev
  [{:keys [in]} & args]
  (cond->> nil
    in   (concat [:in (StringReader. (if (string? in) in (pr-str in)))])
    true (concat args)
    true (apply sh ev-bin)))

(deftest basic-test
  (testing "round trip test"
    (let [expected {:foo/bar 'baz
                    "another" 1.0
                    0         true
                    false     nil
                    #{}       []}]
      (are [arguments] (= expected
                          (-> (apply ev {:in expected} arguments)
                              :out
                              edn/read-string))
        []
        ["--pretty"]
        ["--newline"]
        ["--newline" "--pretty"]))))

(deftest pretty-print-test
  (let [expected {:really.long/key {:to.be/prettified
                                    "Architecto sunt qui in extenetur"}}]
    (is (= (with-out-str (pprint/pprint expected))
           (:out (ev {:in expected} "--pretty"))))))

(deftest file-output-test
  (let [file (str (fs/file *tmp-dir* "output.edn"))
        expected [:test "output" 'file]]
    (ev {:in expected} "--output" file)
    (is (= expected (edn/read-string (slurp file)))))
  (testing "append to output file"
    (let [file (str (fs/file *tmp-dir* "append.edn"))
          expected [:test "append" 'file]]
      (ev {:in expected} "--output" file "--append")
      (ev {:in expected} "--output" file "--append")
      (is (= (with-out-str
               (dotimes [_ 2] (pr expected)))
             (slurp file)))))
  (testing "overwrite output file"
    (let [file (str (fs/file *tmp-dir* "overwrite.edn"))
          expected [:test "overwrite" 'file]]
      (spit file "this should be overwritten.")
      (ev {:in expected} "--output" file)
      (ev {:in expected} "--output" file)
      (is (= (with-out-str (pr expected)) (slurp file))))))

(deftest file-input-test
  (is (= {:test "input"}
         (edn/read-string (slurp "test/test-input.edn"))
         (edn/read-string (:out (ev nil "test/test-input.edn"))))))

(deftest key-type-test
  (let [input {:ns/key  nil
               'ns/sym  nil
               "ns/str" nil
               :key     nil
               'sym     nil
               "str"    nil}]
    (is (= input (-> (ev {:in input}) :out edn/read-string))
        "Key type is preserved when no coversion is specified")
    (are [type expected] (= expected
                            (-> (ev {:in input} "--key-type" type)
                                :out
                                edn/read-string))
      "preserve" input
      "string"   {"ns/key" nil
                  "ns/sym" nil
                  "ns/str" nil
                  "key"    nil
                  "sym"    nil
                  "str"    nil}
      "keyword"  {:ns/key nil
                  :ns/sym nil
                  :ns/str nil
                  :key    nil
                  :sym    nil
                  :str    nil}
      "symbol"   '{ns/key nil
                   ns/sym nil
                   ns/str nil
                   key    nil
                   sym    nil
                   str    nil})))

(deftest key-case-test
  (is (= {:multi-part-key true}
         (-> (ev {:in {:multiPartKey true}} "--key-case" "kebab-case")
             :out
             edn/read-string)))
  (doseq [[case-name conversion-fn] ev/case-conversions]
    (is (= {(conversion-fn :multiPartKey) true}
           (-> (ev {:in {:multiPartKey true}} "--key-case" case-name)
               :out
               edn/read-string))
        (format "%s case conversion" case-name))))

(deftest from-json-test
  (is (= {"test" "input"}
         (edn/read-string
          (:out (ev nil "--from-json" "test/test-input.json")))))
  (is (= {"from" "json"}
         (edn/read-string
          (:out (ev {:in "{\"from\":\"json\"}"} "--from-json"))))))

(deftest to-json-test
  (is (= "{\"to\":\"json\"}"
         (:out (ev {:in {:to :json}} "--to-json"))))
  (is (= "{\n  \"to\" : \"json\"\n}"
         (:out (ev {:in {:to :json}} "--to-json" "--pretty")))
      "pretty printing json")
  (is (= "{\"to/json\":\"qualified/symbol\"}"
         (:out (ev {:in {:to/json 'qualified/symbol}} "--to-json")))
      "converting symbols and keywords"))
